FROM ubuntu AS builder

RUN apt-get update && apt-get install wget git --yes
RUN wget https://repo.anaconda.com/miniconda/Miniconda3-latest-Linux-x86_64.sh \
 && bash ./Miniconda3-latest-Linux-x86_64.sh -p /opt/conda -b \
 && rm -rf ./Miniconda3-latest-Linux-x86-64.sh
RUN /opt/conda/bin/conda create -p /opt/clim/python -c conda-forge iris

FROM ubuntu
COPY --from=builder /opt/clim/python /opt/clim/python
ENV PATH="/opt/clim/python/bin:${PATH}"
RUN mkdir -p /opt/clim/tools
WORKDIR /opt/clim/tools
COPY fetch_station_data.py /opt/clim/tools/
COPY generate_climatology.py /opt/clim/tools/
COPY locate_nearest_station.py /opt/clim/tools/
