# Site-specific temperature climatology

This repository provides tools to compute climatological data for WMO stations from
the [HadISD](https://www.metoffice.gov.uk/hadobs/hadisd/) dataset for use with
[CAiMIRA](https://gitlab.cern.ch/caimira/caimira).


## Disclaimer

This repository is part of the CAiMIRA project, and has not undergone review, approval or certification by competent authorities.
As a result, the output of the assessments using this climatological data cannot be considered as fully endorsed and reliable, namely in the assessment of potential viral concentration.

The software is provided "as is", without warranty of any kind, express or implied, including but not limited to the warranties of merchantability, fitness for a particular purpose and non-infringement.
In no event shall the authors or copyright holders be liable for any claim, damages or other liability, whether in an action of contract, tort or otherwise, arising from, out of or in connection with the software or the use or other dealings in the software.

CERN strives to deploy its know-how and technologies to help solve the challenges arising in the local and global fight against COVID-19.
As a particle physics research organisation, CERN is not in a position to advise on medical research, health or health policy issues.
Any initiative is conducted on a best effort and as-is basis, without liability or warranty.


## Finding weather data for your location:

This repository contains a pre-built docker container which may be used to generate the desired weather data.
You can run the container with:

   $ docker run -it gitlab-registry.cern.ch/caimira/climatology-data

1) Locate your nearest weather station using locate_nearest_station.py
   From within your container, execute the command:
   `python locate_nearest_station.py latitude longitude`
   Where the values for `latitude` and `longitude` are those for your location in decimal format.

   e.g. for Geneva, Switzerland enter `46.20 6.14` or for New York, USA enter `40.71 -74.00`

2) Download the historical dataset associated with that location using fetch_station_data.py
   Execute the command:
   `python fetch_station_data.py station-id filename.nc`
   Where the value for `station-id` is the output from the previous script and `filename.nc` is a friendly name for your location.
   
   e.g. for Geneva, Switzerland enter the weather station ID `067000-99999` and `geneva.nc`

3) Convert the data into hourly averages used by CAiMIRA using generate_climatology.py
   Execute the command:
   `python generate_climatology.py filename.nc data/filename.json`
   Where the value for `filename.nc` is the one used in stage 2 and `data/filename.json` is the output path and filename.

Once you have completed these steps, you can use the .json file with your CAiMIRA instance to provide localised outside temperature profiles.

Weather station names and locations (lat/long) are taken from this file:
https://www.metoffice.gov.uk/hadobs/hadisd/v312_202103p/files/hadisd_station_info_v312_202103p.txt
