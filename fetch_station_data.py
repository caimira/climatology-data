# This module is part of CAiMIRA. Please see the repository at
# https://gitlab.cern.ch/caimira/climatology-data for details of the license and terms of use.

import argparse
import gzip
from pathlib import Path
import urllib.request

parser = argparse.ArgumentParser()
parser.add_argument('station_id')
parser.add_argument('output_filename')
args = parser.parse_args()

fname = f'hadisd.3.1.1.2020f_19310101-20210101_{args.station_id}.nc.gz'
URL = f'https://www.metoffice.gov.uk/hadobs/hadisd/v311_2020f/data/{fname}'
print(f'Fetching URL: {URL}')
req = urllib.request.Request(URL)
req.add_header('User-Agent', 'urllib/0.1')

with urllib.request.urlopen(req) as f:
    content = f.read()

nc_file = gzip.decompress(content)

out_path = Path(args.output_filename)
out_path.parent.mkdir(exist_ok=True)
with out_path.open('wb') as fh:
    fh.write(nc_file)
