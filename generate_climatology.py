# This module is part of CAiMIRA. Please see the repository at
# https://gitlab.cern.ch/caimira/climatology-data for details of the license and terms of use.

import json
from pathlib import Path
import time
import warnings

import iris
import numpy as np

import argparse


parser = argparse.ArgumentParser()
parser.add_argument('filename')
parser.add_argument('output_filename')
args = parser.parse_args()


start = time.time()
with warnings.catch_warnings():
    warnings.simplefilter("ignore")
    temp = iris.load_cube(args.filename, 'temperatures')

times = temp.coord('time').units.num2date(temp.coord('time').points)
temp.add_aux_coord(iris.coords.AuxCoord(long_name='month', points=np.vectorize(lambda t: t.month)(times)), 0)
temp.add_aux_coord(iris.coords.AuxCoord(long_name='hour', points=np.vectorize(lambda t: t.hour)(times)), 0)
years = np.vectorize(lambda t: t.year)(times)
start_idx, end_idx = np.argmax(years >= 1990), np.argmax(years > 2020)

temp = temp[start_idx: end_idx or -1]
print(f'{temp.shape[0]} records between {times[start_idx]} and {times[end_idx-1]}')

# Compute the mean for each hour of each month.
mean_temp_by_month_and_hour_1d = temp.aggregated_by(['hour', 'month'], iris.analysis.MEAN)

# Tidy up the data, first by turning this into a 12 (months) x 24 (hours) dataset
mean_temp_by_month_and_hour = iris.cube.CubeList(
    [mean_temp_by_month_and_hour_1d[i] for i in range(mean_temp_by_month_and_hour_1d.shape[0])]
).merge_cube()
mean_temp_by_month_and_hour.remove_coord('time')
mean_temp_by_month_and_hour.attributes.clear()

# The data will look something like:
# surface_temperature / (degree_Celsius) (hour: 24; month: 12)
#     Dimension coordinates:
#          hour                              x          -
#          month                             -          x


# Turn the data into a suitable JSON structure.
data = {'temperature_per_hour_by_month': {}}
for month_data in mean_temp_by_month_and_hour.slices_over(['month']):
    month_number = int(month_data.coord('month').points[0])  # 1 based, 1=Jan.
    data['temperature_per_hour_by_month'][month_number] = [
        float(v) for v in month_data.data.round(1)
    ]

print(f'Generated JSON in {time.time() - start:0.2f} seconds')
out_path = Path(args.output_filename)
out_path.parent.mkdir(exist_ok=True)
with out_path.open('wt') as fh:
    json.dump(data, fh)
