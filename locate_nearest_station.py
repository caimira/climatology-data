# This module is part of CAiMIRA. Please see the repository at
# https://gitlab.cern.ch/caimira/climatology-data for details of the license and terms of use.

import argparse, csv, urllib.request
import numpy as np
from pathlib import Path
from scipy.spatial import cKDTree


parser = argparse.ArgumentParser()
parser.add_argument('lat')
parser.add_argument('long')
args = parser.parse_args()


print("Coordinates entered (lat, long): ",args.lat,", ", args.long)
search_coords = [args.lat, args.long]

lat=[]
long=[]
station_array=[]
fixed_delimits = [0,12,13, 44,51,60,69,90,91]


print("Loading database of weather stations, looking for local file...")
station_file = Path('hadisd_station_fullinfo_v311_202001p.txt')

if not station_file.exists():
    print("Local file not found, downloading database of weather stations")
    URL = 'https://www.metoffice.gov.uk/hadobs/hadisd/v311_2020f/files/hadisd_station_fullinfo_v311_202001p.txt'
    req = urllib.request.Request(URL)
    req.add_header('User-Agent', 'urllib/0.1')
    with urllib.request.urlopen(req) as f:
        content = f.read()
    with station_file.open('wt') as fh:
        fh.write(content.decode())

for line in station_file.open('rt'):
    start_end_positions=zip(fixed_delimits[:-1], fixed_delimits[1:])
    split_vals=[line[start:end] for start, end in start_end_positions]
    station_location = [split_vals[0], split_vals[2], split_vals[3], split_vals[4]]
    station_array.append(station_location)
    lat.append(split_vals[3])
    long.append(split_vals[4])

tree = cKDTree(np.c_[lat, long])
dd, ii = tree.query(search_coords, k=[1])

print("Nearest weather station ID:")
print(station_array[ii[0]][0])
print("Nearest weather station name:", station_array[ii[0]][1])
print("Nearest weather station coordinates:", station_array[ii[0]][2], ", ", station_array[ii[0]][3])
